package view;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import model.*;
import controller.*;

public class EnigmaFrame extends JFrame {

private static final long serialVersionUID = 8728706163602850776L;
	
	SpinnerModel rotor1Model = new SpinnerNumberModel(0, 0, 25, 1);
	SpinnerModel rotor2Model = new SpinnerNumberModel(0, 0, 25, 1);
	SpinnerModel rotor3Model = new SpinnerNumberModel(0, 0, 25, 1);
	
	JTextArea inputText = new JTextArea();
	JTextArea outputText = new JTextArea();
	
	JSpinner rotor1spinner = new JSpinner(rotor1Model);
	JSpinner rotor2spinner = new JSpinner(rotor2Model);
	JSpinner rotor3spinner = new JSpinner(rotor3Model);
	
	JButton rotorsButton = new JButton("Configurar Rotores");
	JButton reflectorButton = new JButton("Configurar Refletores");
	JButton plugboardButton = new JButton("Configurar Plugboard");
		
	JLabel inputLabel = new JLabel("Entrada:");
	JLabel outputLabel = new JLabel("Saida:");
	
	Plugboard plugboard = new Plugboard();
	Enigma enigma = new Enigma(RotorType.I, RotorType.II, RotorType.III, ReflectorType.B, plugboard);
	
	RotorFrame rotorConfig;
	ReflectorFrame reflectorConfig;
	PlugboardFrame plugboardConfig;
	
	public EnigmaFrame(){
		super("Enigma Machine - Mateus Manuel e Vitor Barbosa");		
		
		rotorConfig = new RotorFrame(enigma, this);
		reflectorConfig = new ReflectorFrame(enigma, this);
		plugboardConfig = new PlugboardFrame(enigma, this);
		
		this.getContentPane().setPreferredSize(new Dimension(760, 365));
		this.pack();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
		getContentPane().setLayout(null);
		
		//this.setSize(760,385);
		//setResizable(false);
		
		setLocationRelativeTo(null);
		
		rotor1spinner.setBounds(10, 15, 40, 30);
		getContentPane().add(rotor1spinner);
		
		rotor2spinner.setBounds(60, 15, 40, 30);
		getContentPane().add(rotor2spinner);
		
		rotor3spinner.setBounds(110, 15, 40, 30);
		getContentPane().add(rotor3spinner);
		
		rotor3spinner.addChangeListener(new ChangeListener(){
			@Override
			public void stateChanged(ChangeEvent e){
				for (;;){
					if ((int) rotor3spinner.getValue() == enigma.getRotor1Positon()){
						break;
					}
					
					if ((int) rotor3spinner.getValue() > enigma.getRotor1Positon()){
						enigma.rotateRot1();
					}
					else{
						enigma.undoRotateRot1();
					}
				}
			}
		});
		
		rotor2spinner.addChangeListener(new ChangeListener(){
			
			@Override
			public void stateChanged(ChangeEvent e){
				for (;;){
					if ((int) rotor2spinner.getValue() == enigma.getRotor2Positon()){
						break;
					}
					
					if ((int) rotor2spinner.getValue() > enigma.getRotor2Positon()){
						enigma.rotateRot2();
						
					}
					else{
						enigma.undoRotateRot2();
					}
				}				
			}
		});
		
		rotor1spinner.addChangeListener(new ChangeListener(){
			
			@Override
			public void stateChanged(ChangeEvent e){
				for (;;){
					if ((int) rotor1spinner.getValue() == enigma.getRotor3Positon()){
						break;
					}
					
					if ((int) rotor1spinner.getValue() > enigma.getRotor3Positon()){
						enigma.rotateRot3();
						
					}
					else{
						enigma.undoRotateRot3();
					}
				}				
			}
		});
		
		rotorsButton.setBounds(170, 15, 170, 30);
		getContentPane().add(rotorsButton);
				
		rotorsButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				rotorConfig.setVisible(true);				
			}
		});
		
		reflectorButton.setBounds(350, 15, 190, 30);
		getContentPane().add(reflectorButton);
		
		reflectorButton.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent e){
				reflectorConfig.setVisible(true);				
			}
		});
		
		plugboardButton.setBounds(550, 15, 190, 30);
		getContentPane().add(plugboardButton);
		
		plugboardButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				plugboardConfig.setVisible(true);				
			}			
		});
	
		inputLabel.setBounds(11, 46, 80, 20);
		getContentPane().add(inputLabel);
		
		inputText.setLineWrap(true);
		inputText.setBounds(10, 65, 732, 120);
		
		getContentPane().add(inputText);
		
		outputLabel.setBounds(10, 194, 60, 20);
		getContentPane().add(outputLabel);
		
		outputText.setLineWrap(true);
		outputText.setEditable(false);
		outputText.setBounds(10, 213, 732, 133);
		getContentPane().add(outputText);
		
		inputText.addKeyListener(new KeyListener(){
			@Override
			public void keyTyped(KeyEvent e){
				processToOutput(e);
			}
			
			@Override
			public void keyPressed(KeyEvent e){
			}
			
			@Override
			public void keyReleased(KeyEvent e){
			}
		});
	}
	
	public void processToOutput(KeyEvent e){
		
		if (e.getKeyChar() == KeyEvent.VK_BACK_SPACE){
			if (outputText.getText().length() > 0){
				if (outputText.getText().charAt(outputText.getText().length() - 1) != ' '){
					enigma.undoRotateRotor();
				}
				
				outputText.setText(outputText.getText().substring(0, outputText.getText().length() - 1));
				rotor3spinner.setValue(enigma.getRotor1Positon());
				rotor2spinner.setValue(enigma.getRotor2Positon());
				rotor1spinner.setValue(enigma.getRotor3Positon());
			}
			
			return;
		}
		
		outputText.setText(outputText.getText() + enigma.fullCharProcess(e.getKeyChar()));
		
		rotor3spinner.setValue(enigma.getRotor1Positon());
		rotor2spinner.setValue(enigma.getRotor2Positon());
		rotor1spinner.setValue(enigma.getRotor3Positon());
	}
	
	//@SuppressWarnings("null")
	public String getClipboardContents(){
		String result = "";
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		Transferable contents = clipboard.getContents(null);
		boolean hasTransferableText = (contents != null) && contents.isDataFlavorSupported(DataFlavor.stringFlavor);
		if (hasTransferableText){
			try{
				result = (String) contents.getTransferData(DataFlavor.stringFlavor);
			}
			catch (UnsupportedFlavorException ex){
				ex.printStackTrace();
			}
			catch (IOException ex){
				ex.printStackTrace();
			}
		}
		return result;
	}		
}
