package view;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

import model.*;
import controller.*;

public class ReflectorFrame extends JFrame {

private static final long serialVersionUID = -1053154317625464019L;
	
	Enigma enigma;
	private EnigmaFrame enigmaFrame;
	
	RotorType Rotor1TypeSelected;
	RotorType Rotor2TypeSelected;
	RotorType Rotor3TypeSelected;

	ReflectorType reflectorType;

	JLabel reflectorLabel = new JLabel("Tipo do refletor:");
	JRadioButton reflectorTypeA = new JRadioButton("A");
	JRadioButton reflectorTypeB = new JRadioButton("B");
	JRadioButton reflectorTypeC = new JRadioButton("C");
	
	JButton saveButton = new JButton("Salvar e Reiniciar");
	
	ReflectorListener reflectorListener = new ReflectorListener();
	
	public ReflectorFrame(Enigma enigma, EnigmaFrame enigmaFrame){
		super("Configurar refletores");
		
		this.enigma = enigma;
		this.enigmaFrame = enigmaFrame;
		
		Rotor1TypeSelected = enigma.getRot1Type();
		Rotor2TypeSelected = enigma.getRot2Type();
		Rotor3TypeSelected = enigma.getRot3Type();
		
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setSize(350, 100);
		setResizable(false);
		setLayout(new FlowLayout());
		setLocationRelativeTo(null);
		
		add(reflectorLabel);
		
		add(reflectorTypeA);
		add(reflectorTypeB);
		add(reflectorTypeC);
		
		reflectorTypeA.setActionCommand("A");
		reflectorTypeB.setActionCommand("B");
		reflectorTypeC.setActionCommand("C");
		
		reflectorTypeA.addActionListener(reflectorListener);
		reflectorTypeB.addActionListener(reflectorListener);
		reflectorTypeC.addActionListener(reflectorListener);
		
		add(saveButton);
		saveButton.addActionListener(new ActionListener(){	
			@Override
			public void actionPerformed(ActionEvent e){
				newEnigma();				
			}
		});
		
	    ButtonGroup group = new ButtonGroup();
	    group.add(reflectorTypeA);
	    group.add(reflectorTypeB);
	    group.add(reflectorTypeC);
	}
	
	public class ReflectorListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e){
			if(e.getActionCommand() == "A"){
				reflectorType = ReflectorType.A;
			}
			
			if(e.getActionCommand() == "B"){
				reflectorType = ReflectorType.B;
			}
			
			if(e.getActionCommand() == "C"){
				reflectorType = ReflectorType.C;
			}			
		}
	}
	
	public void newEnigma(){
		enigma = new Enigma(Rotor1TypeSelected, Rotor2TypeSelected, Rotor3TypeSelected, reflectorType, enigma.getPlugboard());
		setVisible(false);
		enigmaFrame.enigma = enigma;
		enigmaFrame.rotor3spinner.setValue(enigma.getRotor1Positon());
		enigmaFrame.rotor2spinner.setValue(enigma.getRotor2Positon());
		enigmaFrame.rotor1spinner.setValue(enigma.getRotor3Positon());
		enigmaFrame.inputText.setText("");
		enigmaFrame.outputText.setText("");
	}
	
	@Override
	public void setVisible(boolean visible){
		super.setVisible(visible);
		
		reflectorTypeA.setSelected(false);
		reflectorTypeB.setSelected(false);
		reflectorTypeC.setSelected(false);
		
		switch (enigma.getReflectorType()){
			case A:
				reflectorTypeA.setSelected(true);
				break;
			case B:
				reflectorTypeB.setSelected(true);
				break;
			case C:
				reflectorTypeC.setSelected(true);
				break;
			default:
				break;
		}		
	}	
}